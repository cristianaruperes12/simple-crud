<?php

namespace App\Models;

use CodeIgniter\Model;

class ContactModel extends Model
{
    protected $table = 'contact';
    // protected $useTimestamps = true;
    protected $allowedFields = ['name', 'phone_number', 'email'];
}
