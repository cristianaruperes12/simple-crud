<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<div class="container">
    <div class="row">
        <div class="col">
            <h1>Home</h1>
            <p>Welcome to Sistem Information</p>
            <p><i>Please see <b>the SIMPLE CRUD</b> on the contact page...</i></p>
        </div>
    </div>
</div>

<?= $this->endSection(); ?>