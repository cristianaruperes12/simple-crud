<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<div class="container">
    <div class="row">
        <div class="col">
            <h1>Add Contact</h1>

            <form action="/pages/contact/add-contact/save" method="post">
                <?= csrf_field(); ?>
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Name</label>
                    <input type="text" id="name" name="name" class="form-control <?= ($validation->hasError('name')) ? 'is-invalid' : ''; ?>" placeholder="Example" autofocus>
                    <div class="invalid-feedback">
                        <?= $validation->getError('name'); ?>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Phone Number</label>
                    <input type="text" id="phone_number" name="phone_number" class="form-control" placeholder="0812xxxx">
                </div>
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Email</label>
                    <input type="email" id="email" name="email" class="form-control" placeholder="name@example.com">
                </div>

                <button href="" type="submit" class="btn btn-primary">Save</button>
                <br><br>
                <a href="<?= base_url('/pages/contact') ?>">Back to Contact Page</a>
            </form>
        </div>
    </div>
</div>

<?= $this->endSection(); ?>