<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<div class="container">
    <div class="row">
        <div class="col">
            <h1>Detail</h1>

            <form action="/pages/contact/detail/edit/<?= $contact['id']; ?>" method="post" class="d-inline">
                <?= csrf_field(); ?>
                <input type="hidden" name="id" value="<?= $contact['id']; ?>">

                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Name</label>
                    <input type="text" id="name" name="name" class="form-control <?= ($validation->hasError('name')) ? 'is-invalid' : ''; ?>" placeholder="Example" value="<?= $contact['name']; ?>" autofocus>
                    <div class="invalid-feedback">
                        <?= $validation->getError('name'); ?>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Phone Number</label>
                    <input type="text" name="phone_number" class="form-control" value="<?= $contact['phone_number']; ?>">
                </div>
                <div class="mb-3">
                    <label for="exampleFormControlInput1" class="form-label">Email</label>
                    <input type="email" name="email" class="form-control" value="<?= $contact['email']; ?>">
                </div>

                <button type="submit" class="btn btn-warning">Edit</button>

            </form>
            <form action="/pages/contact/detail/delete/<?= $contact['id']; ?>" method="post" class="d-inline">
                <?= csrf_field(); ?>
                <input type="hidden" name="_method" value="DELETE">
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>

            <br><br>
            <a href="<?= base_url('/pages/contact') ?>">Back to Contact Page</a>

        </div>
    </div>
</div>

<?= $this->endSection(); ?>