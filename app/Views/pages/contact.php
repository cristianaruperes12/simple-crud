<?= $this->extend('layout/template'); ?>

<?= $this->section('content'); ?>
<div class="container">
    <div class="row">
        <div class="col">
            <h1>Contact list</h1>

            <a href="/pages/contact/add-contact" class="btn btn-primary mb-3">Add Contact</a>

            <?php if (session()->getFlashData('message')) : ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->getFlashData('message'); ?>
                </div>
            <?php endif; ?>

            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Phone Number</th>
                        <th scope="col">Email</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($contact as $c) : ?>
                        <tr>
                            <th scope="row"><?= $c['id']; ?></th>
                            <td><?= $c['name']; ?></td>
                            <td><?= $c['phone_number']; ?></td>
                            <td><?= $c['email']; ?></td>
                            <td>
                                <a href="/pages/contact/detail/<?= $c['id']; ?>" class="btn btn-success">Detail</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

        </div>
    </div>
</div>

<?= $this->endSection(); ?>