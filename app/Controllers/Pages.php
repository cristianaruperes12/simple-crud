<?php

namespace App\Controllers;

use App\Models\ContactModel;
use CodeIgniter\HTTP\Request;

class Pages extends BaseController
{
	public function index()
	{
		// echo view('layout/header');
		// echo view('home');
		// echo view('layout/footer');

		$data = [
			'title' => 'Home'
		];
		return view('/pages/home', $data);
	}

	public function about()
	{
		$data = [
			'title' => 'About'
		];
		return view('pages/about', $data);
	}

	public function contact()
	{
		$contactModel = new ContactModel();
		$contact = $contactModel->findAll();

		$data = [
			'title' => 'Contact',
			'contact' => $contact
		];

		return view('pages/contact', $data);
	}

	public function detail($id)
	{
		$contactModel = new ContactModel();
		$contact = $contactModel->where(['id' => $id])->first();

		$data = [
			'title' => 'Detail',
			'contact' => $contact,
			'validation' => \Config\Services::validation()
		];

		return view('pages/detail_contact', $data);
	}

	public function createContact()
	{
		$data = [
			'title' => 'Add Contact',
			'validation' => \Config\Services::validation()
		];

		return view('/pages/add_contact', $data);
	}

	public function saveContact()
	{
		// validasi input

		if (!$this->validate([
			'name' => 'required|is_unique[contact.name]'
		])) {
			$validation = \Config\Services::validation();
			return redirect()->to('pages/contact/add-contact')->withInput()->with('validation', $validation);
		}
		$contactModel = new ContactModel();

		$contactModel->save([
			'name' => $this->request->getVar('name'),
			'phone_number' => $this->request->getVar('phone_number'),
			'email' => $this->request->getVar('email')
		]);

		session()->setFlashdata('message', 'contact has been added');

		return redirect()->to('pages/contact');
	}

	public function deleteContact($id)
	{
		$contactModel = new ContactModel();

		$contactModel->delete($id);

		session()->setFlashdata('message', 'contact has been removed');

		return redirect()->to('pages/contact');
	}

	public function editContact($id)
	{
		$contactModel = new ContactModel();
		$contact = $contactModel->where(['id' => $id])->first();
		$oldName = $contact['name'];

		// check name unique
		if ($oldName == $this->request->getVar('name')) {
			$rule_name = 'required';
		} else {
			$rule_name = 'required|is_unique[contact.name]';
		}

		// validasi input
		if (!$this->validate([
			'name' => $rule_name
		])) {
			$validation = \Config\Services::validation();
			return redirect()->to('pages/contact/detail/' . $this->request->getVar('id'))->withInput()->with('validation', $validation);
		}


		$contactModel->save([
			'id' => $id,
			'name' => $this->request->getVar('name'),
			'phone_number' => $this->request->getVar('phone_number'),
			'email' => $this->request->getVar('email')
		]);

		session()->setFlashdata('message', 'contact has been updated');

		return redirect()->to('pages/contact');
	}
}
